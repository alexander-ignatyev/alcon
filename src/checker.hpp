#pragma once

#include <stdlib.h>
#include <time.h>

#include <algorithm>

template <typename C>
class SortingChecker {
public:
    C &container_;

    template <typename ForwardIterator>
    bool issorted(ForwardIterator first, ForwardIterator last) {
        ForwardIterator next(first);
        for(++next; next != last; ++first, ++next) {
            if(*next < *first) {
                return false;
            }
        }
        return true;
    }
public:
    typedef typename C::iterator iterator;

    SortingChecker(C &container): container_(container) {
        std::generate(container_.begin(), container_.end(), rand);
    }

    template <typename F>
    double check(F sortfunc) {
        C c(container_);
        clock_t start = clock();
        sortfunc(c.begin(), c.end());
        clock_t end = clock();
        double time = double(end - start) / CLOCKS_PER_SEC;
        if(!issorted(c.begin(), c.end())) {
            time += 1e6;
        }
        return time;
    }
};
