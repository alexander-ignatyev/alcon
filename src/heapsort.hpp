#pragma once

#include <algorithm>

namespace heapsort_int
{
inline int parent(int n) { return (--n) >> 1; }
inline int left_child(int n) { return (n << 1) | 1; }
inline int right_child(int n) { return (++n) << 1; }

//recursive implementation is more effective
//#define HEAPSORT_RECURSIVE

#ifdef HEAPSORT_RECURSIVE
#define max_heapify max_heapify_recursive
#else
#define max_heapify max_heapify_iterative
#endif

template <typename RndAccessIterator>
void max_heapify_recursive(RndAccessIterator begin, int i, int heapsize) {
    int left = left_child(i);
    int right = right_child(i);
    int largest = i;
    if (left < heapsize && begin[left] > begin[largest]) {
        largest = left;
    }
    if (right < heapsize && begin[right] > begin[largest]) {
        largest = right;
    }
    if (largest != i) {
        std::swap(begin[i], begin[largest]);
        max_heapify(begin, largest, heapsize);
    }
}

template <typename RndAccessIterator>
void max_heapify_iterative(RndAccessIterator a, int i, int heapsize) {
    int left;
    while ((left = left_child(i)) < heapsize) {
        const int right = left + 1;
        const int largest = (right < heapsize && a[right] > a[left]) ? right : left;
        if (a[i] > a[largest]) 
            break;
        std::swap(a[i], a[largest]);
        i = largest;
    }
}


template <typename RndAccessIterator>
int build_max_heap(RndAccessIterator begin, int heapsize) {
    for (int i = parent(heapsize - 1); i >= 0; --i) {
        max_heapify(begin, i, heapsize);
    }
    return heapsize;
}
} //end namespace heapsort_int

template <typename RndAccessIterator>
void heapsort(RndAccessIterator first, RndAccessIterator last) {
    using namespace heapsort_int;
    int heapsize = build_max_heap(first, last - first);
    while (heapsize > 1) {
        std::swap(first[0], first[--heapsize]);
        max_heapify(first, 0, heapsize);
    }
}
