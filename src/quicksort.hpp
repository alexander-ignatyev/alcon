#pragma once
#include <iterator>
namespace quicksork_intl
{
template <typename BiDirIterator>
BiDirIterator partition(BiDirIterator first, BiDirIterator last) {
    --last;
    typename std::iterator_traits<BiDirIterator>::reference pivot = *last;
    BiDirIterator pos(first);
    for (; first != last; ++first) {
        if (*first <= pivot) {
            std::iter_swap(first, pos);
            ++pos;
        }
    }
    std::iter_swap(pos, last);
    return pos;
}
} //end namespace quicksork_intl

template <typename BiDirIterator>
void quicksort(BiDirIterator first, BiDirIterator last) {
    using namespace quicksork_intl;
    if (first != last) {
        BiDirIterator pos = partition(first, last);
        quicksort(first, pos);
        quicksort(++pos, last);
    }
}
