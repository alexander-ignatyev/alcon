#include "checker.hpp"
#include "heapsort.hpp"
#include "quicksort.hpp"

#include <algorithm>
#include <iostream>
#include <vector>

template <typename Iter>
void stdsort(Iter first, Iter last) {
    std::sort(first, last);
}

template <typename Iter>
void stdheapsort(Iter first, Iter last) {
    std::make_heap(first, last);
    std::sort_heap(first, last);
}

int main(int argc, char *argv[]) {
    int size = 1000000;
    if(argc > 1) {
        size = atoi(argv[1]);
    }
    std::vector<int> array(size);

    typedef SortingChecker<std::vector<int> > Checker;
    Checker checker(array);
    double timeHeap = checker.check(heapsort<Checker::iterator >);
    std::cout << "heapsort: " << timeHeap << std::endl;
    double timeStd = checker.check(stdsort<Checker::iterator >);
    std::cout << "std::sort: " << timeStd << std::endl;
    double timeQuick = checker.check(quicksort<Checker::iterator >);
    std::cout << "quicksort: " << timeQuick << std::endl;
    double timeStdHeap = checker.check(stdheapsort<Checker::iterator >);
    std::cout << "stdheapsort: " << timeStdHeap << std::endl;
    std::cout << "heapsort/std::sort: " << (timeHeap / timeStd) << std::endl;
    std::cout << "quicksort/std::sort: " << (timeQuick / timeStd) << std::endl;
    std::cout << "heapsort/std::heap: " << (timeHeap / timeStdHeap) << std::endl;
    return 0;
}
